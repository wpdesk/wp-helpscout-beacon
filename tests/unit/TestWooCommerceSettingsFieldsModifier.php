<?php

use WP_Mock\Tools\TestCase;
use WPDesk\Beacon\Beacon\WooCommerceSettingsFieldsModifier;

/**
 * Tests WooCommerceSettingsFieldsModifier.
 */
class TestWooCommerceSettingsFieldsModifier extends TestCase {

	const FIELD_TITLE = 'Field title';
	const FIELD_TYPE = 'type';
	const FIELD_TYPE_TEXT = 'text';

	/**
	 * Set up.
	 */
	public function setUp() {
		\WP_Mock::setUp();
	}

	/**
	 * Tear down.
	 */
	public function tearDown() {
		\WP_Mock::tearDown();
	}

	/**
	 * @dataProvider fields_to_test
	 */
	public function test_append_beacon_search_data_to_fields( $form_fields, $expected_fields ) {
		$modifier = new WooCommerceSettingsFieldsModifier();

		$this->assertEquals( $expected_fields, $modifier->append_beacon_search_data_to_fields( $form_fields ) );

	}

	/**
	 * Data provider for fields tests.
	 * Returns array of fields and expected fields after modifier.
	 *
	 * @return array
	 */
	public function fields_to_test() {
		return array(
			array(
				'form_fields' => array(
					'form_field' => array(
						WooCommerceSettingsFieldsModifier::FIELD_TITLE => self::FIELD_TITLE,
						self::FIELD_TYPE                               => self::FIELD_TYPE_TEXT,
					)
				),
				'expected_fields' => array(
					'form_field' => array(
						WooCommerceSettingsFieldsModifier::FIELD_TITLE             => self::FIELD_TITLE,
						self::FIELD_TYPE                                           => self::FIELD_TYPE_TEXT,
						WooCommerceSettingsFieldsModifier::FIELD_CLASS             => WooCommerceSettingsFieldsModifier::CLASS_HS_BEACON_SEARCH,
						WooCommerceSettingsFieldsModifier::FIELD_CUSTOM_ATTRIBUTES => array(
							WooCommerceSettingsFieldsModifier::DATA_BEACON_SEARCH  => self::FIELD_TITLE,
						)
					)
				)
			),
			array(
				'form_fields' => array(
					'form_field' => array(
						WooCommerceSettingsFieldsModifier::FIELD_TITLE => self::FIELD_TITLE,
						WooCommerceSettingsFieldsModifier::FIELD_CLASS => 'test-class',
						self::FIELD_TYPE                               => self::FIELD_TYPE_TEXT,
					)
				),
				'expected_fields' => array(
					'form_field' => array(
						WooCommerceSettingsFieldsModifier::FIELD_TITLE             => self::FIELD_TITLE,
						WooCommerceSettingsFieldsModifier::FIELD_CLASS             => 'test-class ' . WooCommerceSettingsFieldsModifier::CLASS_HS_BEACON_SEARCH,
						self::FIELD_TYPE                                           => self::FIELD_TYPE_TEXT,
						WooCommerceSettingsFieldsModifier::FIELD_CUSTOM_ATTRIBUTES => array(
							WooCommerceSettingsFieldsModifier::DATA_BEACON_SEARCH  => self::FIELD_TITLE,
						)
					)
				)
			),
			array(
				'form_fields' => array(
					'form_field' => array(
						self::FIELD_TYPE => self::FIELD_TYPE_TEXT,
					)
				),
				'expected_fields' => array(
					'form_field' => array(
						self::FIELD_TYPE => self::FIELD_TYPE_TEXT,
					)
				)
			),
		);
	}

}