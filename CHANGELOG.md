## [1.6.1] - 2023-01-02
### Fixed
- Beacon RTL

## [1.6.0] - 2022-09-16
### Added
- helpscout-beacon/should-display-beacon filter

## [1.5.0] - 2022-08-30
### Added
- de_DE translations

## [1.4.0] - 2022-08-11
### Added
- pl_PL, en_AU, en_GB and en_CA translations

## [1.3.2] - 2020-02-25
### Fixed
- read data-beacon_search as attribute

## [1.3.1] - 2020-10-21
### Fixed
- should_display_beacon method

## [1.3.0] - 2020-10-14
### Added
- ability to pass image data for beacon

## [1.2.0] - 2020-04-16
### Added
- WooCommerce settings fields modifier for beacon search

## [1.1.1] - 2020-04-16
### Fixed
- removed search script

## [1.1.0] - 2020-04-16
### Added
- beacon pro without confirmation
- beacon auto search

## [1.0.0] - 2020-04-14
### Added
- first stable version