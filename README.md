[![pipeline status](https://gitlab.com/wpdesk/wp-helpscout-beacon/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-helpscout-beacon/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-helpscout-beacon/badges/master/coverage.svg)](https://gitlab.com/wpdesk/wp-helpscout-beacon/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-helpscout-beacon/v/stable)](https://packagist.org/packages/wpdesk/wp-helpscout-beacon) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-helpscout-beacon/downloads)](https://packagist.org/packages/wpdesk/wp-helpscout-beacon) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-helpscout-beacon/v/unstable)](https://packagist.org/packages/wpdesk/wp-helpscout-beacon) 
[![License](https://poser.pugx.org/wpdesk/wp-helpscout-beacon/license)](https://packagist.org/packages/wpdesk/wp-helpscout-beacon)

WordPress Library for HelpScout Beacon integration
===================================================


## Requirements

PHP 5.6 or later.

## Composer

You can install the bindings via [Composer](http://getcomposer.org/). Run the following command:

```bash
composer require --dev wpdesk/wp-helpscout-beacon
```

To use the bindings, use Composer's [autoload](https://getcomposer.org/doc/01-basic-usage.md#autoloading):

```php
require_once 'vendor/autoload.php';
```

## Usage

```php
add_action( 'plugin/ready', function () {
    if ( is_admin() && get_option( 'disable_beacon', 'no' ) !== 'yes' ) {
        ( new Beacon(
            '6057086f-4b25-4e12-8735-fbc556d2dc05',
            new BeaconShopMagicShouldShowStrategy(),
            $vendor_url . '/wp-helpscout-beacon/assets/'
        ) )->hooks();

    }
} );
```

## Project documentation

PHPDoc: https://wpdesk.gitlab.io/wp-helpscout-beacon/index.html  